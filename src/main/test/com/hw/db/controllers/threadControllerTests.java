package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Message;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class threadControllerTests {
    private User user;
    private com.hw.db.models.Thread thread;

    @BeforeEach
    @DisplayName("Entities creation test")
    void createEnititiesTest() {
        user = new User("some", 
                "some@email.mu", 
                "Innokenty Smoktunovsky", 
                "kesha");
        thread = new com.hw.db.models.Thread("some",
                new Timestamp(System.currentTimeMillis()),
                "forum",
                "message",
                "slug",
                "title",
                1);
        thread.setId(1);
    }

    @Test
    @DisplayName("Check id or slug success id")
    void checkIdOrSlugSuccessId() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(thread, controller.CheckIdOrSlug("1"), "Success");
        }
    }

    @Test
    @DisplayName("Check id or slug success slug")
    void checkIdOrSlugSuccessSlug() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(thread, controller.CheckIdOrSlug("slug"), "Success");
        }
    }

    @Test
    @DisplayName("Create post success")
    void createPostSuccess() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                List<Post> posts = new ArrayList<>();
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message",
                        1,
                        1,
                        Boolean.TRUE));
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message2",
                        1,
                        1,
                        Boolean.TRUE));

                userDAOMock.when(() -> UserDAO.Info("nickname")).thenReturn(user);
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts),
                        controller.createPost("slug", posts),
                        "Result for succeeding forum creation");
            }
        }
    }

    @Test
    @DisplayName("Create post not found")
    void createPostNotFound() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                List<Post> posts = new ArrayList<>();
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message",
                        1,
                        1,
                        Boolean.TRUE));
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message2",
                        1,
                        1,
                        Boolean.TRUE));

                userDAOMock.when(() -> UserDAO.Info("nickname")).thenReturn(user);
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenThrow(new DataAccessException("") {
                });

                threadController controller = new threadController();
                assertEquals(
                        ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("")).getStatusCode(),
                        controller.createPost("slug", posts).getStatusCode(), "Success");
            }
        }
    }

    @Test
    @DisplayName("Create post conflict")
    void createPostConflict() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                List<Post> posts = new ArrayList<>();
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message",
                        1,
                        1,
                        Boolean.TRUE));
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message2",
                        1,
                        1,
                        Boolean.TRUE));

                List<User> users = new ArrayList<>();
                users.add(user);
                users.add(user);

                userDAOMock.when(() -> UserDAO.Info("nickname")).thenReturn(user);
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.createPosts(thread, posts, users)).thenThrow(new DataAccessException("") {
                });

                threadController controller = new threadController();
                assertEquals(
                        ResponseEntity.status(HttpStatus.CONFLICT).body(new Message("")).getStatusCode(),
                        controller.createPost("slug", posts).getStatusCode(), "Thread conflict");
            }
        }
    }

    @Test
    @DisplayName("Posts success")
    void postsSuccess() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                Integer limit = 1;
                Integer since = 2;
                String sort = "sort";
                Boolean desc = true;

                List<Post> posts = new ArrayList<>();
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message",
                        1,
                        1,
                        Boolean.TRUE));
                posts.add(new Post("nickname",
                        new Timestamp(System.currentTimeMillis()),
                        "from",
                        "message2",
                        1,
                        1,
                        Boolean.TRUE));

                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.getPosts(thread.getId(), limit, since, sort, desc)).thenReturn(posts);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts),
                        controller.Posts("slug", limit, since, sort, desc),
                        "Success");
            }
        }
    }

    @Test
    @DisplayName("Posts not found")
    void postsNotFound() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenThrow(new DataAccessException("") {
            });

            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("")).getStatusCode(),
                    controller.Posts("slug", 1, 1, "sort", true).getStatusCode(), "Success");
        }
    }

    @Test
    @DisplayName("Change success")
    void changeSuccess() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            com.hw.db.models.Thread newThread = new com.hw.db.models.Thread(
                    "some",
                    new Timestamp(System.currentTimeMillis()),
                    "forum",
                    "message",
                    "slug",
                    "title",
                    1);
            newThread.setId(2);

            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(thread.getId().toString())).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(),
                    controller.change("slug", newThread).getStatusCode(),
                    "Success");
        }
    }

    @Test
    @DisplayName("Change not found")
    void changeNotFound() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            com.hw.db.models.Thread newThread = new com.hw.db.models.Thread(
                    "some",
                    new Timestamp(System.currentTimeMillis()),
                    "forum",
                    "message",
                    "slug",
                    "title",
                    1);
            newThread.setId(2);

            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(thread.getId().toString())).thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.change(thread, newThread)).thenThrow(new DataAccessException("") {
            });

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(thread).getStatusCode(),
                    controller.change("slug", newThread).getStatusCode(),
                    "Success");
        }
    }

    @Test
    @DisplayName("Info success")
    void infoSuccess() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(),
                    controller.info("slug").getStatusCode(),
                    "Success");
        }
    }

    @Test
    @DisplayName("Info not found")
    void infoNotFound() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenThrow(new DataAccessException("") {
            });

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(thread).getStatusCode(),
                    controller.info("slug").getStatusCode(),
                    "Success");
        }
    }

    @Test
    @DisplayName("Create vote success")
    void createVoteSuccess() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                Vote vote = new Vote(user.getNickname(), 1);

                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
                userDAOMock.when(() -> UserDAO.Info(user.getNickname())).thenReturn(user);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(),
                        controller.createVote("slug", vote).getStatusCode(),
                        "Success");
            }
        }
    }

    @Test
    @DisplayName("Create vote not found")
    void createVoteNotFound() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                Vote vote = new Vote(user.getNickname(), 1);

                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenThrow(new DataAccessException("") {
                });
                threadDAOMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
                userDAOMock.when(() -> UserDAO.Info(user.getNickname())).thenReturn(user);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(thread).getStatusCode(),
                        controller.createVote("slug", vote).getStatusCode(),
                        "Success");
            }
        }
    }

    @Test
    @DisplayName("Create vote conflict")
    void createVoteConflict() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                Vote vote = new Vote(user.getNickname(), 1);

                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.createVote(thread, vote)).thenThrow(new DuplicateKeyException(""));
                threadDAOMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenThrow(new DuplicateKeyException(""));
                userDAOMock.when(() -> UserDAO.Info(user.getNickname())).thenReturn(user);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).body(thread).getStatusCode(),
                        controller.createVote("slug", vote).getStatusCode(),
                        "Success");
            }
        }
    }
}
